package com.phone.dao;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.phone.entity.Phone;

public interface PhoneRepository  extends CrudRepository<Phone, Long> {

	Set<Phone> findAllByCustomerId(long customerId);
}
