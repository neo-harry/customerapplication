package com.phone.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phone.dao.PhoneRepository;
import com.phone.entity.Phone;

@Service
public class PhoneServiceImpl implements PhoneService {

	@Autowired
	private PhoneRepository phoneRepository;

	@Override
	public Phone save(Phone phone) {
		return phoneRepository.save(phone);
	}

	@Override
	public Set<Phone> getphonesByCustomerId(long customeId) {
		return phoneRepository.findAllByCustomerId(customeId);
	}

}
