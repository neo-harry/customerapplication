package com.phone.service;

import java.util.Set;

import com.phone.entity.Phone;

public interface PhoneService {
	
	Phone save(Phone phone);
	
	Set<Phone> getphonesByCustomerId(long customeId);
}
