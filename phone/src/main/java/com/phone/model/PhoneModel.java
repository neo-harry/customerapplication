package com.phone.model;

import com.phone.enums.NumberType;

public class PhoneModel {
	
	private String numType;
	
	private long number;

	public PhoneModel (byte numType, long number) {
		this.numType = NumberType.getNameByType(numType);
		this.number = number;
	}
	
	public String getNumType() {
		return numType;
	}

	public void setNumType(String numType) {
		this.numType = numType;
	}

	public long getNumber() {
		return number;
	}

	public void setNumber(long number) {
		this.number = number;
	}

}
