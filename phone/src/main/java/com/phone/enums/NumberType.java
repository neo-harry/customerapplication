package com.phone.enums;

public enum NumberType {
	 MOBILE("mobile", (byte) 1),
	 OFFICE("office", (byte) 2),
	 HOME("home", (byte) 3);
	 
	private byte type;
	private String name;
	
	NumberType(String name, byte type) {
		this.name = name;
		this.type = type;
	}

	public byte getType() {
		return type;
	}

	public void setType(byte type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static String getNameByType(byte type) {
		String name = "";
		for (NumberType numberType : NumberType.values()) {
			if (numberType.getType() == type) {
				name = numberType.getName();
				break;
			}
		}
		return name;
	}
}
