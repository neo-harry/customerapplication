package com.phone.controller;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.phone.entity.Phone;
import com.phone.model.PhoneModel;
import com.phone.service.PhoneService;

@RestController
@RequestMapping("/phone")
public class phoneController {
	@Autowired
	private PhoneService phoneService;
	
	@PostMapping
	public ResponseEntity<Phone> createPhone(@RequestBody Phone phone) {
		phone = phoneService.save(phone);
		return new ResponseEntity<Phone>(phone, HttpStatus.OK);
	}
	
	@GetMapping("/{customerId}")
	public ResponseEntity<Set<PhoneModel>> getphones(@PathVariable("customerId") long customerId) {
		Set<PhoneModel> phonesModel = null;
		Set<Phone> phones = phoneService.getphonesByCustomerId(customerId);
		if (phones.isEmpty())
			return new ResponseEntity<Set<PhoneModel>>(phonesModel, HttpStatus.NO_CONTENT);
		phonesModel = phones.stream().map(phone -> phone.convertToModel()).collect(Collectors.toSet());
		return new ResponseEntity<Set<PhoneModel>>(phonesModel, HttpStatus.OK);
	}
}
