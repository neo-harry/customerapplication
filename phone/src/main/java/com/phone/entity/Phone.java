package com.phone.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.phone.model.PhoneModel;

@Entity
public class Phone {

	@Id
	@GeneratedValue
	private long id;
	
	private byte numType;
	
	private long number;
	
	private long customerId;
	
	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public byte getNumType() {
		return numType;
	}

	public void setNumType(byte numType) {
		this.numType = numType;
	}

	public long getNumber() {
		return number;
	}

	public void setNumber(long number) {
		this.number = number;
	}

	public PhoneModel convertToModel() {
		return new PhoneModel(this.numType, this.number);
	}
	
}
