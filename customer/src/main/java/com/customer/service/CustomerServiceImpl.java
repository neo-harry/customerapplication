package com.customer.service;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.customer.dao.CustomerRepository;
import com.customer.entity.Customer;
import com.customer.model.AddressModel;
import com.customer.model.PhoneModel;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${url.phone.service}")
	private String phoneUrl;
	
	@Value("${url.address.service}")
	private String addressUrl;
	
	@Override
	public Customer save(Customer customer) {
		return customerRepository.save(customer);
	}

	@Override
	public Customer getCustomerById(long id) {
		Optional<Customer> customer = customerRepository.findById(id);
		return customer.isPresent() ? customer.get() : null;
	}

	@Override
	public Set<PhoneModel> getPhonesByCustomerId(long id) {
		return Arrays.stream(restTemplate.getForObject(phoneUrl + id, PhoneModel[].class)).collect(Collectors.toSet());
	}

	@Override
	public Set<AddressModel> getAddressesByCustomerId(long id) {
		return Arrays.stream(restTemplate.getForObject(addressUrl + id, AddressModel[].class)).collect(Collectors.toSet());
	}

}
