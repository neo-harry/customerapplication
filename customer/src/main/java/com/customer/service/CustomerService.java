package com.customer.service;

import java.util.Set;

import com.customer.entity.Customer;
import com.customer.model.AddressModel;
import com.customer.model.PhoneModel;

public interface CustomerService {
	
	Customer save(Customer customer);
	
	Customer getCustomerById(long id);
	
	Set<PhoneModel>getPhonesByCustomerId(long id);

	Set<AddressModel> getAddressesByCustomerId(long id);
}
