package com.customer.model;

import java.util.Set;

public class CustomerModel {

	private long id;
	
	private String firstName;
	
	private String middleName;
	
	private String lastName;
	
	private Integer age;

	private Set<PhoneModel> phones;
	
	private Set<AddressModel> addresses;	
	
	public Set<PhoneModel> getPhones() {
		return phones;
	}

	public void setPhones(Set<PhoneModel> phones) {
		this.phones = phones;
	}

	public Set<AddressModel> getAddresses() {
		return addresses;
	}

	public void setAddresses(Set<AddressModel> addresses) {
		this.addresses = addresses;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
}
