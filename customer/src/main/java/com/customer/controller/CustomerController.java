package com.customer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.customer.entity.Customer;
import com.customer.model.CustomerModel;
import com.customer.service.CustomerService;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	@Autowired
	private CustomerService customerService;
	
	@PostMapping
	public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {
		customer = customerService.save(customer);
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
	}
	
	@GetMapping("/{customerId}")
	public ResponseEntity<CustomerModel> getCustomer(@PathVariable("customerId") long customerId) {
		CustomerModel customerModel = new CustomerModel();
		Customer customer = customerService.getCustomerById(customerId);
		if (customer == null)
			return new ResponseEntity<CustomerModel>(customerModel, HttpStatus.NO_CONTENT);
		System.out.println(customer.getId());
		customerModel.setId(customer.getId());
		customerModel.setAge(customer.getAge());
		customerModel.setFirstName(customer.getFirstName());
		customerModel.setMiddleName(customer.getMiddleName());
		customerModel.setLastName(customer.getLastName());
		customerModel.setAddresses(customerService.getAddressesByCustomerId(customerId));
		customerModel.setPhones(customerService.getPhonesByCustomerId(customerId));
		return new ResponseEntity<CustomerModel>(customerModel, HttpStatus.OK);
	}
}
