package com.address.controller;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.address.entity.Address;
import com.address.model.AddressModel;
import com.address.service.AddressService;

@RestController
@RequestMapping("/address")
public class addressController {
	
	@Autowired
	private AddressService addressService;
	
	@PostMapping
	public ResponseEntity<Address> createAddress(@RequestBody Address address) {
		address = addressService.save(address);
		return new ResponseEntity<Address>(address, HttpStatus.OK);
	}
	
	@GetMapping("/{customerId}")
	public ResponseEntity<Set<AddressModel>> getAddresses(@PathVariable("customerId") long customerId) {
		Set<AddressModel> addressesModel = null;
		Set<Address> addresses = addressService.getAddressesByCustomerId(customerId);
		if (addresses.isEmpty())
			return new ResponseEntity<Set<AddressModel>>(addressesModel, HttpStatus.NO_CONTENT);
		addressesModel = addresses.stream().map(address -> address.convertToModel()).collect(Collectors.toSet());
		return new ResponseEntity<Set<AddressModel>>(addressesModel, HttpStatus.OK);
	}

}
