package com.address.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.address.dao.AddressRepository;
import com.address.entity.Address;

@Service
public class AddressServiceImpl implements AddressService {

	@Autowired
	private AddressRepository addressRepository;
	
	@Override
	public Address save(Address address) {
		return addressRepository.save(address);
	}

	@Override
	public Set<Address> getAddressesByCustomerId(long customerId) {
		return addressRepository.findAllByCustomerId(customerId);
	}

}
