package com.address.service;

import java.util.Set;

import com.address.entity.Address;

public interface AddressService {
	
	Address save(Address address);
	
	Set<Address> getAddressesByCustomerId(long customerId);
}
