package com.address.dao;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.address.entity.Address;

@Repository
public interface AddressRepository extends CrudRepository<Address, Long> {
	
	Set<Address> findAllByCustomerId(long customerId);
}
